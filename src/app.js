import React from "react";
import Editor from "./editor/index.js";
import { RecoilRoot } from "recoil";

export default function App() {
  return (
    <div>
      {/* <div>Landing Page</div>
      <div>Sign In</div> */}
      <RecoilRoot >
        <Editor />
      </RecoilRoot>
    </div>
  );
}
