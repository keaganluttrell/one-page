import React from "react";
import ReactDOM from "react-dom";
import CssBaseline from "@mui/material/CssBaseline";
import App from "./app.js";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { blue, red } from "@mui/material/colors";

const theme = createTheme({
  palette: {
    primary: blue,
    secondary: {
      main: red[400],
    },
  },
  typography: {
    htmlFontSize: 12,
    fontSize: 10,
  },
});

ReactDOM.render(
  <>
    <CssBaseline />
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </>,
  document.getElementById("root")
);
