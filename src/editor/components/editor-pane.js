import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import { useRecoilState } from "recoil";
import { currTemplateState } from "../editor-atoms.js";
import { Typography, Card, CardContent } from "@mui/material";
import TopicEditor from "./topic-list.js";

export default function EditorPane() {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);

  function DisplayTopics() {
    console.log(currTemplate);
    if (currTemplate && currTemplate.topics.length === 0) {
      return (
        <Card variant="outlined" sx={{width: "50%"}}>
          <CardContent>
            <Typography fontStyle="italic">
              You have no topics for this template
            </Typography>
          </CardContent>
        </Card>
      );
    } else {
      return <TopicEditor />;
    }
  }

  return (
    <Grid
      container
      item
      direction="column"
      // textAlign="center"
      justifyContent="center"
      alignContent="center"
      xs={12}
      margin={5}
    >
      <DisplayTopics />
    </Grid>
  );
}
