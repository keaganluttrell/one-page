import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { currTemplateState, openTopicTabState } from "../editor-atoms";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { TextField, Container, Typography } from "@mui/material";
import Modal from "@mui/material/Modal";
import { getNewTopic } from "../api/get-requests";

const style = {
  position: "absolute",
  top: "30%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "none",
  boxShadow: 24,
  p: 1,
};

export default function BasicModal({ title, setTitle, open, setOpen }) {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [openTopicTab, setOpenTopicTab] = useRecoilState(openTopicTabState);
  const handleChange = (e) => setTitle(e.target.value);
  const handleClose = () => setOpen(false);

  function handleSubmit(e) {
    e.preventDefault();
    const newTopic = getNewTopic(title);
    let newTemplate = JSON.parse(JSON.stringify(currTemplate));
    newTemplate.topics.push(newTopic);
    setCurrTemplate(newTemplate);
    setOpenTopicTab(newTemplate.topics.length - 1);
    setOpen(false);
    setTitle("");
  }

  return (
    <div>
      <Modal
        open={open}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <Box sx={style}>
          <Typography>Add New Topic</Typography>
          <TextField
            fullWidth
            id="outlined"
            label=""
            name="title"
            multiline
            variant="outlined"
            value={title}
            placeholder={"Enter a title"}
            size="small"
            onChange={handleChange}
          />
          <Container
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 1,
            }}
          >
            <Button variant="contained" size="small" onClick={handleSubmit}>
              SUBMIT
            </Button>
            <Button
              variant="contained"
              size="small"
              color="secondary"
              onClick={handleClose}
            >
              CLOSE
            </Button>
          </Container>
        </Box>
      </Modal>
    </div>
  );
}
