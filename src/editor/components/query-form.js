import React, { useEffect, useState } from "react";
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  TextField,
  RadioGroup,
  MenuItem,
  Select,
  Radio,
  Button,
} from "@mui/material";
import { useRecoilValue, useRecoilState } from "recoil";
import { selectedTemplateTypeState, currTemplateState } from "../editor-atoms";
import { getNewTemplate, getTemplateTitles } from "../api/get-requests";

export default function QueryForm({
  operation,
  setOperation,
  title,
  setTitle,
  placeholder,
}) {
  const selectedTemplateType = useRecoilValue(selectedTemplateTypeState);
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [templateTitles, setTemplateTitles] = useState([]);

  useEffect(() => {
    const titles = getTemplateTitles();
    setTemplateTitles(titles);
  }, [operation]);

  function handleRadioChange(e) {
    setTitle("");
    setOperation(e.target.value);
    setCurrTemplate(null);
  }

  return (
    <form
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "end",
        marginTop: "16px",
      }}
    >
      <FormControl component="fieldset">
        <RadioGroup
          row
          aria-label="operation"
          name="radio-buttons-group"
          required={true}
          onChange={handleRadioChange}
        >
          <FormControlLabel
            value="add"
            control={<Radio size="small" checked={operation === "add"} />}
            label="Add"
          />
          <FormControlLabel
            value="edit"
            control={<Radio size="small" checked={operation === "edit"} />}
            label="Edit"
          />
        </RadioGroup>
      </FormControl>
      {(operation === "add" && (
        <TextField
          id="outlined"
          label="Title"
          variant="outlined"
          value={title}
          placeholder={placeholder}
          size="small"
          onChange={(e) => setTitle(e.target.value)}
        />
      )) ||
        (operation === "edit" && (
          <Select
            labelId={"query-form-edit"}
            id={"query-form-edit-select"}
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            size="small"
            sx={{ minWidth: 194 }}
          >
            <MenuItem value="Select one" disabled>
              Select one
            </MenuItem>
            ;
            {templateTitles.map((title) => {
              return (
                <MenuItem value={title} key={title}>
                  {title}
                </MenuItem>
              );
            })}
          </Select>
        ))}

      <Button
        variant="text"
        size="small"
        type="submit"
        onClick={(e) => {
          e.preventDefault();
          if (operation === "add") {
            console.log("add");
            const template = getNewTemplate(selectedTemplateType, title);
            setCurrTemplate(template);
          } else {
            console.log("edit");
            const template = getNewTemplate(selectedTemplateType, title);
            setCurrTemplate(template);
          }
        }}
      >
        Go
      </Button>
    </form>
  );
}
