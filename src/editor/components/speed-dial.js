import React from "react";
import Box from "@mui/material/Box";
import SpeedDial from "@mui/material/SpeedDial";
import SpeedDialIcon from "@mui/material/SpeedDialIcon";
import SpeedDialAction from "@mui/material/SpeedDialAction";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import AddIcon from "@mui/icons-material/Add";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import DynamicFeedIcon from "@mui/icons-material/DynamicFeed";

export default function BasicSpeedDial({
  addQuestion,
  topicIndex,
  deleteTopic,
}) {
  const actions = [
    { icon: <KeyboardArrowDownIcon />, name: "Move down" },
    {
      icon: (
        <AddIcon
          color="primary"
          onClick={() => {
            addQuestion(topicIndex);
          }}
        />
      ),
      name: "Add Question",
    },
    {
      icon: (
        <DeleteOutlineIcon
          color="secondary"
          onClick={() => {
            deleteTopic(topicIndex);
          }}
        />
      ),
      name: "Delete Topic",
    },
    { icon: <KeyboardArrowUpIcon />, name: "Move up" },
  ];

  return (
    <Box sx={{ height: 80, transform: "translateZ(0px)", flexGrow: 1 }}>
      <SpeedDial
        ariaLabel="SpeedDial basic example"
        sx={{ position: "absolute", bottom: 10, right: 10 }}
        icon={<SpeedDialIcon openIcon={<DynamicFeedIcon size="small" />} />}
        FabProps={{ size: "small" }}
      >
        {actions.map((action) => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            arrow
          />
        ))}
      </SpeedDial>
    </Box>
  );
}
