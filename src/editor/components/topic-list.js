import React, { useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import SpeedDial from "./speed-dial.js";
import Accordion from "@mui/material/Accordion";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import { currTemplateState, openTopicTabState } from "../editor-atoms.js";
import { getNewQuestionBlock } from "../api/get-requests.js";
import QuestionEditor from "./question-editor.js";

export default function TopicEditor() {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [openTopicTab, setOpenTopicTab] = useRecoilState(openTopicTabState);
  const [expanded, setExpanded] = useState(true);

  function addQuestion(topicIndex) {
    const newQuestion = getNewQuestionBlock();
    let newTemplate = JSON.parse(JSON.stringify(currTemplate));
    newTemplate.topics[topicIndex].questions.push(newQuestion);
    setCurrTemplate(newTemplate);
  }

  function deleteTopic(index) {
    let newTemplate = JSON.parse(JSON.stringify(currTemplate));
    newTemplate.topics.splice(index, 1);
    setCurrTemplate(newTemplate);
  }

  const { topics } = currTemplate;
  return topics.map((topic, topicIndex) => {
    return (
      <Accordion
        key={topicIndex}
        sx={{ width: "80%", margin: 0, padding: 0 }}
        expanded={topicIndex === openTopicTab && expanded}
        variant="outlined"
        onChange={() => {
          if (topicIndex === openTopicTab) {
            setExpanded(!expanded);
          } else {
            setOpenTopicTab(topicIndex);
            setExpanded(true);
          }
        }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls={topic.title + "-content"}
          id={topic.title + "-content"}
          sx={{ margin: 0 }}
        >
          <Typography>{topic.title}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {(Array.isArray(topic.questions) &&
            topic.questions.length > 0 &&
            topic.questions.map((questionBlock, questionIndex) => {
              console.log("ok");
              return (
                <QuestionEditor
                  key={questionBlock.type + "-" + questionIndex}
                  topicIndex={topicIndex}
                  questionBlock={questionBlock}
                  questionIndex={questionIndex}
                />
              );
            })) || (
            <Typography fontStyle="italic">
              No question for this topic yet
            </Typography>
          )}
          <SpeedDial
            addQuestion={addQuestion}
            topicIndex={topicIndex}
            deleteTopic={deleteTopic}
          />
        </AccordionDetails>
      </Accordion>
    );
  });
}
