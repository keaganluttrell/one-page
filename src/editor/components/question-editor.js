import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { currTemplateState } from "../editor-atoms";
import {
  Card,
  CardContent,
  CardActions,
  IconButton,
  Tooltip,
} from "@mui/material";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import SaveIcon from "@mui/icons-material/Save";
import DeleteIcon from "@mui/icons-material/Delete";
import QuestionDisplay from "./question-display";

export default function QuestionEditor({
  topicIndex,
  questionBlock,
  questionIndex,
}) {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [editQuestion, setEditQuestion] = useState(questionBlock);

  function addQuestion() {
    let newTemplate = JSON.parse(JSON.stringify(currTemplate));
    newTemplate.topics[topicIndex].questions[questionIndex] = editQuestion;
    setCurrTemplate(newTemplate);
  }

  function deleteQuestion() {
    let newTemplate = JSON.parse(JSON.stringify(currTemplate));
    newTemplate.topics[topicIndex].questions.splice(questionIndex, 1);
    setCurrTemplate(newTemplate);
  }

  return (
    <Card
      variant="outlined"
      component={"div"}
      sx={{
        marginBottom: 1,
        padding: 0,
      }}
    >
      <CardActions sx={{ padding: 0 }}>
        <Tooltip title="Move up" arrow placement="top">
          <IconButton>
            <KeyboardArrowUpIcon fontSize="small" color="primary" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Move down" arrow placement="top">
          <IconButton>
            <KeyboardArrowDownIcon fontSize="small" color="primary" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Save Question" arrow placement="top">
          <IconButton onClick={addQuestion}>
            <SaveIcon fontSize="small" color="primary" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Delete Question" arrow placement="top">
          <IconButton onClick={deleteQuestion}>
            <DeleteIcon fontSize="small" color="secondary" />
          </IconButton>
        </Tooltip>
      </CardActions>
      <CardContent component={"div"}>
        <QuestionDisplay
          topicIndex={topicIndex}
          questionIndex={questionIndex}
          editQuestion={editQuestion}
          setEditQuestion={setEditQuestion}
        />
      </CardContent>
    </Card>
  );
}
