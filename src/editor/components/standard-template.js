import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import QueryForm from "./query-form";
import { useRecoilState } from "recoil";
import { currTemplateState } from "../editor-atoms.js";
import Toolbar from "./toolbar.js";
import EditorPane from "./editor-pane.js";

export default function StandardTemplate() {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [operation, setOperation] = useState("add");
  const [title, setTitle] = useState("");

  function DisplayEditorPane() {
    if (currTemplate !== null) {
      return <EditorPane />;
    } else {
      return null;
    }
  }

  return (
    <Grid container>
      <Grid
        container
        item
        xs={12}
        direction="row"
        justifyContent="space-between"
      >
        <QueryForm
          operation={operation}
          setOperation={setOperation}
          title={title}
          setTitle={setTitle}
          placeholder="Medical History eg."
        />
        <Toolbar />
      </Grid>
      <DisplayEditorPane />
    </Grid>
  );
}
