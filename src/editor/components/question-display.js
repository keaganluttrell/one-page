import React from "react";
import {
  Select,
  TextField,
  FormControl,
  InputLabel,
  Checkbox,
  MenuItem,
  FormControlLabel,
  Grid,
} from "@mui/material";
import EditQuestionOptions from "./edit-question-options";

const questionTypes = ["text", "checkbox", "radio", "select"];

export default function QuestionDisplay({
  editQuestion,
  setEditQuestion,
  questionIndex,
}) {

  const { type, question, required, options } = editQuestion;
  function handleChange(e) {
    setEditQuestion({
      ...editQuestion,
      [e.target.name]: e.target.value,
    });
  }

  return (
    <form>
      <Grid container display="flex" direction="row" spacing={2}>
        <Grid item xs={6}>
          <FormControl fullWidth sx={{ marginBottom: 2 }}>
            <InputLabel id={"question-type-label-" + questionIndex}>
              Type
            </InputLabel>
            <Select
              fullWidth
              name="type"
              labelId={"question-type-label-" + questionIndex}
              id={"question-select-" + questionIndex}
              value={type}
              label="Type"
              onChange={handleChange}
              size="small"
            >
              <MenuItem value="Select one" disabled>
                Select one
              </MenuItem>
              ;
              {questionTypes.map((qType) => {
                return (
                  <MenuItem value={qType} key={qType}>
                    {qType}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <TextField
            fullWidth
            id="outlined"
            label="Question"
            name="question"
            multiline
            variant="outlined"
            value={question}
            placeholder={"Enter a question"}
            size="small"
            onChange={handleChange}
          />
          <FormControlLabel
            name="required"
            value={required}
            control={
              <Checkbox
                checked={required}
                onChange={() => {
                  setEditQuestion({
                    ...editQuestion,
                    required: !required,
                  });
                }}
              />
            }
            label="Required?"
          />
        </Grid>
        <Grid
          item
          xs={6}
          container
          display="flex"
          direction="column"
          alignItems="start"
        >
          {type === "text" ? null : (
            <EditQuestionOptions
              options={options}
              editQuestion={editQuestion}
              setEditQuestion={setEditQuestion}
            />
          )}
        </Grid>
      </Grid>
    </form>
  );
}
