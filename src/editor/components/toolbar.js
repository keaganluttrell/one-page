import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import SaveIcon from "@mui/icons-material/Save";
import DeleteIcon from "@mui/icons-material/Delete";
import ToolTip from "@mui/material/Tooltip";
import { currTemplateState } from "../editor-atoms.js";
import TopicModal from "./add-topic-modal.js";

export default function EditorPane() {
  const [currTemplate, setCurrTemplate] = useRecoilState(currTemplateState);
  const [title, setTitle] = useState("");
  const [open, setOpen] = useState(false);
  if (currTemplate === null) return null;

  function addTopic() {
    setOpen(true);
  }

  return (
    <Box sx={{ "& > :not(style)": { m: 1 } }}>
      <ToolTip title="Add a new topic" arrow placement="top">
        <Fab
          color="primary"
          aria-label="add"
          size="small"
          style={{ marginRight: "15px" }}
          onClick={addTopic}
        >
          <AddIcon />
        </Fab>
      </ToolTip>
      <ToolTip title="Save template" arrow placement="top">
        <Fab
          aria-label="save"
          color="success"
          size="small"
          style={{ marginRight: "15px" }}
        >
          <SaveIcon />
        </Fab>
      </ToolTip>
      <ToolTip title="Delete template" arrow placement="top">
        <Fab aria-label="delete" color="secondary" size="small">
          <DeleteIcon />
        </Fab>
      </ToolTip>
      <TopicModal
        title={title}
        setTitle={setTitle}
        open={open}
        setOpen={setOpen}
      />
    </Box>
  );
}
