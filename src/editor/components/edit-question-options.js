import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import AddIcon from "@mui/icons-material/Add";
import {
  OutlinedInput,
  Paper,
  FormControl,
  IconButton,
  Chip,
} from "@mui/material";

const ListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

export default function EditQuestionOptions({
  options,
  editQuestion,
  setEditQuestion,
}) {
  const [chips, setChips] = useState(options);
  const [optionName, setOptionName] = useState("");

  useEffect(() => {
    setChips(options);
  }, [options]);

  const handleDelete = (option) => () => {
    setChips((chips) => chips.filter((chip) => chip !== option));
  };
  function handleChange(e) {
    setOptionName(e.target.value);
  }

  function handleEnter(e) {
    if (e.key === "Enter" || e.type === "click") {
      if (optionName.length > 0) {
        const newOptions = [...chips, optionName.trim()];
        setChips(newOptions);
        setOptionName("");
        setEditQuestion({
          ...editQuestion,
          options: newOptions,
        });
      }
    }
  }

  return (
    <>
      <FormControl variant="outlined" fullWidth>
        <OutlinedInput
          variant="standard"
          id={"options-filed"}
          placeholder="Options"
          fullWidth
          value={optionName}
          onChange={handleChange}
          required={true}
          size="small"
          onKeyDown={handleEnter}
          endAdornment={
            <IconButton
              style={{
                display: "flex",
                margin: 0,
                padding: 0,
              }}
              color="primary"
              size="small"
              onClick={handleEnter}
            >
              <AddIcon />
            </IconButton>
          }
        />
      </FormControl>
      <Paper
        variant="outlined"
        sx={{
          display: "flex",
          justifyContent: "flex-start",
          flexWrap: "wrap",
          listStyle: "none",
          width: "100%",
          p: 0.8,
          m: "1 0",
        }}
        component="ul"
      >
        {chips.map((data) => {
          return (
            <ListItem key={data}>
              <Chip
                label={data}
                onDelete={handleDelete(data)}
                color="default"
              />
            </ListItem>
          );
        })}
        {chips.length === 0 ? "No options" : null}
      </Paper>
    </>
  );
}
