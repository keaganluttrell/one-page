import { atom } from "recoil";
import { getNewTemplate, getNewTopic } from "./api/get-requests.js";

export const selectedTemplateTypeState = atom({
  key: "selectedTemplateState",
  default: "standard",
});

const temp = getNewTemplate("standard", "Medical History");
const topics = getNewTopic("General Info");
temp.topics.push(topics);

export const currTemplateState = atom({
  key: "currentTemplateState",
  default: null
});

export const openTopicTabState = atom({
  key: "openTopicTabState",
  default: 0,
});
