export function getNewQuestionBlock(type = "text") {
  const answer = type === "checkbox" ? [] : "";

  return {
    type,
    question: "",
    answer,
    required: true,
    options: [],
  };
}

export function getNewTopic(topic) {
  return {
    title: topic,
    questions: [
      {
        answer: "",
        options: [],
        question: "Enter a question",
        required: true,
        type: "text",
      },
      {
        answer: [],
        options: ["a", "b", "c"],
        question: "Enter a question",
        required: true,
        type: "checkbox",
      },
      {
        answer: "",
        options: ["a", "b", "c"],
        question: "Enter a question",
        required: true,
        type: "radio",
      },
      {
        answer: "",
        options: ["a", "b", "c"],
        question: "Enter a question",
        required: true,
        type: "select",
      },
    ],
  };
}

export function getNewTemplate(template, title) {
  if (template === "standard") {
    return {
      type: template,
      title,
      topics: [getNewTopic("General Info")]
    };
  }
}

export function getFormByTypeAndTitle(template, title) {
  return getNewTemplate(template, title);
}

export function getTemplateTitles() {
  return ["Review of Systems", "Medical History"];
}
