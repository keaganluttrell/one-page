import React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Grid from "@mui/material/Grid";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import StandardTemplate from "./components/standard-template";
import { useRecoilState } from "recoil";
import { selectedTemplateTypeState } from "./editor-atoms";

export default function Editor() {
  const [selectedTemplateType, setSelectedTemplateType] = useRecoilState(
    selectedTemplateTypeState
  );

  const handleChange = (event, newValue) => {
    setSelectedTemplateType(newValue);
  };

  return (
    <Grid>
      <TabContext value={selectedTemplateType}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="Standard" value="standard" />
            <Tab label="Complaint/Exam" value="complaint" />
          </TabList>
        </Box>
        <TabPanel value="standard" style={{ padding: "8px 20px" }}>
          <StandardTemplate />
        </TabPanel>
        <TabPanel value="complaint">Item Two</TabPanel>
      </TabContext>
    </Grid>
  );
}
