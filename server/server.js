require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const path = require("path");
const IntakeList = require("./database/models/intake-list.js");

const STATIC_FILES = path.resolve(__dirname, "../public");
console.log(STATIC_FILES);
const PORT = process.env.PORT;
const app = express();

app.use(morgan("tiny"));
app.use(express.static(STATIC_FILES));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(PORT, () => {
  console.log("listening on port", PORT);
});

app.get("/api/intake", async (req, res) => {
  const data = await IntakeList.findOne();
  res.status(200).send(data);
});
