const { model, Schema } = require("mongoose");
const db = require("../db.js");

const intakeListSchema = new Schema({
  intakeList: [String],
});

const IntakeList = model("IntakeList", intakeListSchema);

module.exports = IntakeList;
