require("dotenv").config();
const mongooseConnection = require("mongoose");

mongooseConnection.connect(process.env.MONGO_URI, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  user: process.env.MONGO_USER,
  pass: process.env.MONGO_PASSWORD,
});

mongooseConnection.connection.on("connected", () => {
  console.log("Mongoose connected to MongoDB");
});

module.exports = mongooseConnection;